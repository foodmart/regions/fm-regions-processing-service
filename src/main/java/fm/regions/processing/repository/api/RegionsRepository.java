package fm.regions.processing.repository.api;

import fm.regions.processing.model.entity.Region;
import fm.regions.processing.model.request.SearchRequest;

import java.util.List;
import java.util.Optional;

public interface RegionsRepository {
    Optional<Region> findRegionById(int regionId);

    List<Region> findRegionsByIds(List<Integer> regionsIds);

    List<Region> findRegions(SearchRequest request, int page, int size);

    List<Region> findRegions(SearchRequest request);

    long deleteRegion(int regionId);
    long deleteRegions(SearchRequest request);
}
