package fm.regions.processing.repository.api;

import fm.regions.processing.model.request.SearchRequest;
import org.springframework.data.mongodb.core.query.Query;

public interface QueryBuilder {

    Query build(SearchRequest request);
}
