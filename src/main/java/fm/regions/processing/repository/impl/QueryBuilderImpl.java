package fm.regions.processing.repository.impl;

import fm.regions.processing.model.request.SearchRequest;
import fm.regions.processing.model.type.LocalDateTimeType;
import fm.regions.processing.model.type.StringType;
import fm.regions.processing.model.type.Type;
import fm.regions.processing.repository.api.QueryBuilder;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.time.ZoneOffset;

import static fm.regions.processing.constant.RegionEntityConstant.Fields.*;

@Service
public class QueryBuilderImpl implements QueryBuilder {
    @Override
    public Query build(SearchRequest request) {
        Criteria criteria = new Criteria();
        addTypeCriteria(criteria, request.getSalesCity(), SALES_CITY_FIELD_NAME);
        addTypeCriteria(criteria, request.getSalesCityProvince(), SALES_STATE_PROVINCE_FIELD_NAME);
        addTypeCriteria(criteria, request.getSalesDistrict(), SALES_DISTRICT_FIELD_NAME);
        addTypeCriteria(criteria, request.getSalesDistrictId(), SALES_DISTRICT_ID_FIELD_NAME);
        addTypeCriteria(criteria, request.getSalesRegions(), SALES_REGION_FIELD_NAME);
        addTypeCriteria(criteria, request.getSalesCountry(), SALES_COUNTRY_FIELD_NAME);
        return Query.query(criteria);
    }

    private void addTypeCriteria(Criteria criteria, Type<?> type, String fieldName) {
        if (type != null) {
            if (type instanceof StringType stringType) {
                if (stringType.isExact()) {
                    criteria.and(fieldName).is(stringType.getValue());
                } else {
                    criteria.and(fieldName).regex(stringType.getValue());
                }
            } else if (type instanceof LocalDateTimeType dateTimeType) {
                criteria.and(fieldName).is(dateTimeType.getValue().atZone(ZoneOffset.systemDefault()).toInstant());
            } else {
                criteria.and(fieldName).is(type.getValue());
            }
        }
    }
}
