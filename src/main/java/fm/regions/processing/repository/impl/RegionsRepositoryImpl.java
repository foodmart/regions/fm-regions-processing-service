package fm.regions.processing.repository.impl;

import fm.regions.processing.model.entity.Region;
import fm.regions.processing.model.request.SearchRequest;
import fm.regions.processing.repository.api.QueryBuilder;
import fm.regions.processing.repository.api.RegionsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

import static fm.regions.processing.constant.RegionEntityConstant.Fields.REGION_ID_FIELD_NAME;

@Repository
@RequiredArgsConstructor
public class RegionsRepositoryImpl implements RegionsRepository {

    private final MongoTemplate mongoTemplate;

    private final QueryBuilder queryBuilder;

    @Override
    public Optional<Region> findRegionById(int regionId) {
        Criteria criteria = Criteria.where(REGION_ID_FIELD_NAME).is(regionId);
        Query query = Query.query(criteria);
        return mongoTemplate.find(query, Region.class).stream().findFirst();
    }

    @Override
    public List<Region> findRegionsByIds(List<Integer> regionsIds) {
        Criteria criteria = Criteria.where(REGION_ID_FIELD_NAME).in(regionsIds);
        Query query = Query.query(criteria);
        return mongoTemplate.find(query, Region.class);
    }

    @Override
    public List<Region> findRegions(SearchRequest request, int page, int size) {
        Query query = queryBuilder.build(request);
        query = query.with(PageRequest.of(page, size));
        return mongoTemplate.find(query, Region.class);
    }

    @Override
    public List<Region> findRegions(SearchRequest request) {
        Query query = queryBuilder.build(request);
        return mongoTemplate.find(query, Region.class);
    }

    @Override
    public long deleteRegion(int regionId) {
        Criteria criteria = Criteria.where(REGION_ID_FIELD_NAME).is(regionId);
        Query query = Query.query(criteria);
        return mongoTemplate.remove(query, Region.class).getDeletedCount();
    }

    @Override
    public long deleteRegions(SearchRequest request) {
        Query query = queryBuilder.build(request);
        return mongoTemplate.remove(query).getDeletedCount();
    }
}
