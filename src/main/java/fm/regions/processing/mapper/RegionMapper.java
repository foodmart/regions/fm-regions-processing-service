package fm.regions.processing.mapper;

import fm.common.core.api.mapper.Mapper;
import fm.regions.processing.model.entity.Region;
import fm.regions.processing.model.response.RegionResponse;
import org.springframework.stereotype.Component;

@Component
public class RegionMapper implements Mapper<Region, RegionResponse> {
    @Override
    public RegionResponse map(Region region) {
        return RegionResponse.builder()
                .regionId(region.getRegionId())
                .salesCity(region.getSalesCity())
                .salesStateProvince(region.getSalesStateProvince())
                .salesCountry(region.getSalesCountry())
                .salesRegions(region.getSalesRegions())
                .salesDistrict(region.getSalesDistrict())
                .salesDistrictId(region.getSalesDistrictId())
                .build();
    }
}
