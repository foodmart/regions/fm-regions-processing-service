package fm.regions.processing.mapper;

import fm.common.core.api.mapper.Mapper;
import fm.regions.processing.model.response.RegionResponse;
import fm.regions.processing.model.response.RegionsResponse;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RegionsMapper implements Mapper<List<RegionResponse>, RegionsResponse> {
    @Override
    public RegionsResponse map(List<RegionResponse> regions) {
        return RegionsResponse.builder()
                .regions(regions)
                .build();
    }
}
