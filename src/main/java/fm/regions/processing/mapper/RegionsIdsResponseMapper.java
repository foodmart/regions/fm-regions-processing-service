package fm.regions.processing.mapper;

import fm.common.core.api.mapper.Mapper;
import fm.regions.processing.model.entity.Region;
import fm.regions.processing.model.response.RegionsIdsResponse;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RegionsIdsResponseMapper implements Mapper<List<Region>, RegionsIdsResponse> {
    @Override
    public RegionsIdsResponse map(List<Region> regions) {
        return RegionsIdsResponse.builder()
                .regionsIds(regions.stream()
                        .map(Region::getRegionId)
                        .toList())
                .build();
    }
}
