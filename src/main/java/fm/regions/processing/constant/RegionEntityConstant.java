package fm.regions.processing.constant;

public class RegionEntityConstant {

    public static final String COLLECTION_NAME = "region";

    public static class Fields {

        public static final String REGION_ID_FIELD_NAME = "region_id";
        public static final String SALES_CITY_FIELD_NAME = "sales_city";
        public static final String SALES_STATE_PROVINCE_FIELD_NAME = "sales_state_province";
        public static final String SALES_DISTRICT_FIELD_NAME = "sales_district";
        public static final String SALES_REGION_FIELD_NAME = "sales_region";
        public static final String SALES_COUNTRY_FIELD_NAME = "sales_country";
        public static final String SALES_DISTRICT_ID_FIELD_NAME = "sales_district_id";

    }
}
