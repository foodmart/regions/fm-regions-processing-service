package fm.regions.processing.service.impl;

import fm.regions.processing.exception.RegionNotFoundException;
import fm.regions.processing.model.request.SearchRequest;
import fm.regions.processing.repository.api.RegionsRepository;
import fm.regions.processing.service.api.RegionsDeleteService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RegionsDeleteServiceImpl implements RegionsDeleteService {

    private final RegionsRepository regionsRepository;

    @Override
    public void deleteByRegionId(int regionId) {
        final long isDeleted = regionsRepository.deleteRegion(regionId);
        if (isDeleted == 0) {
            throw new RegionNotFoundException(String.format("The requested region id: %d  to be deleted is not found", regionId));
        }
    }

    @Override
    public long deleteRegions(SearchRequest request) {
        return regionsRepository.deleteRegions(request);
    }
}
