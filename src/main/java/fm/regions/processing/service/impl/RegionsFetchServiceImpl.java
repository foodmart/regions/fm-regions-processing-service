package fm.regions.processing.service.impl;

import fm.common.core.api.mapper.Mapper;
import fm.common.core.api.mapper.MapperProvider;
import fm.common.core.model.header.CommonContext;
import fm.regions.processing.exception.RegionNotFoundException;
import fm.regions.processing.model.entity.Region;
import fm.regions.processing.model.request.SearchRequest;
import fm.regions.processing.model.response.RegionResponse;
import fm.regions.processing.model.response.RegionsIdsResponse;
import fm.regions.processing.model.response.RegionsResponse;
import fm.regions.processing.repository.api.RegionsRepository;
import fm.regions.processing.service.api.RegionsFetchService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class RegionsFetchServiceImpl implements RegionsFetchService {

    private final RegionsRepository regionsRepository;

    private final Mapper<Region, RegionResponse> regionMapper;

    private final MapperProvider mapperProvider;

    private final CommonContext commonContext;

    @Override
    public RegionResponse findRegionById(int regionId) {
        Optional<Region> regionOpt = regionsRepository.findRegionById(regionId);
        if (regionOpt.isPresent()) {
            return mapperProvider.map(regionOpt.get(), RegionResponse.class);
        }
        throw new RegionNotFoundException();
    }

    @Override
    public RegionsResponse findRegionsByIds(List<Integer> regionsIds) {
        return mapperProvider.map(regionsRepository.findRegionsByIds(regionsIds)
                .stream()
                .map(regionMapper::map)
                .toList(), RegionsResponse.class);
    }

    @Override
    public RegionsIdsResponse findRegionsIds(SearchRequest request) {
        return mapperProvider.map(regionsRepository.findRegions(request), RegionsIdsResponse.class);
    }

    @Override
    public RegionsResponse findRegions(SearchRequest request) {
        return mapperProvider.map(regionsRepository.findRegions(request, commonContext.getPage(),
                        commonContext.getSize())
                .stream()
                .map(regionMapper::map)
                .toList(), RegionsResponse.class);
    }
}
