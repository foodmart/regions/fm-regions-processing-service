package fm.regions.processing.service.api;

import fm.regions.processing.model.request.SearchRequest;

public interface RegionsDeleteService {

    void deleteByRegionId(int regionId);

    long deleteRegions(SearchRequest request);


}
