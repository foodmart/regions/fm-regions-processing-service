package fm.regions.processing.service.api;

import fm.regions.processing.model.request.SearchRequest;
import fm.regions.processing.model.response.RegionResponse;
import fm.regions.processing.model.response.RegionsIdsResponse;
import fm.regions.processing.model.response.RegionsResponse;

import java.util.List;

public interface RegionsFetchService {

    RegionResponse findRegionById(int regionId);

    RegionsResponse findRegionsByIds(List<Integer> regionsIds);

    RegionsIdsResponse findRegionsIds(SearchRequest request);

    RegionsResponse findRegions(SearchRequest request);
}
