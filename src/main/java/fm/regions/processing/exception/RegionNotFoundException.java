package fm.regions.processing.exception;

import fm.common.core.exception.ResourcesException;

public class RegionNotFoundException extends ResourcesException {

    public RegionNotFoundException() {
    }

    public RegionNotFoundException(String message) {
        super(message);
    }
}
