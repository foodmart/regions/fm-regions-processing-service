package fm.regions.processing.model.request;

import fm.regions.processing.model.type.StringType;
import fm.regions.processing.model.type.Type;
import lombok.Data;

@Data
public class SearchRequest {

    private StringType salesCity;

    private StringType salesCityProvince;

    private StringType salesDistrict;

    private StringType salesRegions;

    private StringType salesCountry;

    private Type<Integer> salesDistrictId;
}
