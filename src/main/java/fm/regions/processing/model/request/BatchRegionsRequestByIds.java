package fm.regions.processing.model.request;

import lombok.Data;

import java.util.List;

@Data
public class BatchRegionsRequestByIds {

    private List<Integer> regionsIds;
}
