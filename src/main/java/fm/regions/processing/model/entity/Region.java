package fm.regions.processing.model.entity;

import fm.regions.processing.constant.RegionEntityConstant;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(RegionEntityConstant.COLLECTION_NAME)
@Data
public class Region {

    @Id
    private String id;

    @Field(RegionEntityConstant.Fields.REGION_ID_FIELD_NAME)
    @Indexed(unique = true)
    private int regionId;

    @Field(RegionEntityConstant.Fields.SALES_CITY_FIELD_NAME)
    private String salesCity;

    @Field(RegionEntityConstant.Fields.SALES_STATE_PROVINCE_FIELD_NAME)
    private String salesStateProvince;

    @Field(RegionEntityConstant.Fields.SALES_DISTRICT_FIELD_NAME)
    private String salesDistrict;

    @Field(RegionEntityConstant.Fields.SALES_REGION_FIELD_NAME)
    private String salesRegions;

    @Field(RegionEntityConstant.Fields.SALES_COUNTRY_FIELD_NAME)
    private String salesCountry;

    @Field(RegionEntityConstant.Fields.SALES_DISTRICT_ID_FIELD_NAME)
    private int salesDistrictId;


}
