package fm.regions.processing.model.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RegionResponse {

    private int regionId;

    private String salesCity;

    private String salesStateProvince;

    private String salesDistrict;

    private String salesRegions;

    private String salesCountry;

    private int salesDistrictId;
}
