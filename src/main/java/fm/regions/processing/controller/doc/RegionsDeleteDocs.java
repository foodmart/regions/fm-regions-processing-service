package fm.regions.processing.controller.doc;

import fm.common.core.model.annoation.CommonParams;
import fm.common.core.model.response.ResponseCoreEntity;
import fm.regions.processing.model.request.SearchRequest;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/delete")
@Tag(name = "Region Delete Controller", description = "Contains endpoints to delete regions entities")
public interface RegionsDeleteDocs {

    @DeleteMapping("/{region_id}")
    @Operation(summary = "Delete region by region id",
            description = "It deletes the region permanently specified by the given region id.")
    @CommonParams
    ResponseCoreEntity<Void> deleteByRegionId(@PathVariable("region_id") int regionId);

    @PostMapping
    @Operation(summary = "Delete region by region request",
            description = "It matches all regions by the requested fields and them delete them permanently.")
    @CommonParams
    ResponseCoreEntity<Long> deleteRegions(@RequestBody SearchRequest searchRequest);


}
