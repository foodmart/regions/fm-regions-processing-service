package fm.regions.processing.controller.doc;

import fm.common.core.model.annoation.CommonParams;
import fm.common.core.model.annoation.Pageable;
import fm.common.core.model.response.ResponseCoreEntity;
import fm.regions.processing.model.request.BatchRegionsRequestByIds;
import fm.regions.processing.model.request.SearchRequest;
import fm.regions.processing.model.response.RegionResponse;
import fm.regions.processing.model.response.RegionsIdsResponse;
import fm.regions.processing.model.response.RegionsResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/fetch")
@Tag(name = "Region Fetch Controller", description = "Contains endpoints to fetch regions entities")
public interface RegionsFetchDocs {

    @GetMapping("/{region_id}")
    @Operation(summary = "Fetch region by region id",
            description = "It returns the region information for the requested region identifier.")
    @CommonParams
    ResponseCoreEntity<RegionResponse> findRegionById(@PathVariable("region_id") int regionId);

    @PostMapping("/batch")
    @Operation(summary = "Fetch region by identifiers",
            description = "It returns the regions information for the requested region identifiers.")
    @CommonParams
    ResponseCoreEntity<RegionsResponse> findRegionsByIds(@RequestBody BatchRegionsRequestByIds request);

    @PostMapping("/search")
    @Operation(summary = "Fetch regions by region request",
            description = "It returns the regions information based on the request fields.")
    @CommonParams
    @Pageable
    ResponseCoreEntity<RegionsResponse> findRegions(@RequestBody SearchRequest request);

    @PostMapping("/search/ids")
    @Operation(summary = "Fetch region identifiers",
            description = "It returns the region identifiers based on the request fields.")
    @CommonParams
    ResponseCoreEntity<RegionsIdsResponse> findRegionsIds(@RequestBody SearchRequest request);
}
