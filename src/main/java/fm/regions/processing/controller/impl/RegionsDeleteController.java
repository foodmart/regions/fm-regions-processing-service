package fm.regions.processing.controller.impl;

import fm.common.core.controller.BaseController;
import fm.common.core.model.response.ResponseCoreEntity;
import fm.regions.processing.controller.doc.RegionsDeleteDocs;
import fm.regions.processing.model.request.SearchRequest;
import fm.regions.processing.service.api.RegionsDeleteService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class RegionsDeleteController extends BaseController implements RegionsDeleteDocs {

    private final RegionsDeleteService regionsDeleteService;

    @Override
    public ResponseCoreEntity<Void> deleteByRegionId(int regionId) {
        regionsDeleteService.deleteByRegionId(regionId);
        return response(HttpStatus.NO_CONTENT);
    }

    @Override
    public ResponseCoreEntity<Long> deleteRegions(SearchRequest searchRequest) {
        return response(regionsDeleteService.deleteRegions(searchRequest), HttpStatus.ACCEPTED);
    }

}
