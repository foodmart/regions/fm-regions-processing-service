package fm.regions.processing.controller.impl;

import fm.common.core.controller.BaseController;
import fm.common.core.model.response.ResponseCoreEntity;
import fm.regions.processing.controller.doc.RegionsFetchDocs;
import fm.regions.processing.model.request.BatchRegionsRequestByIds;
import fm.regions.processing.model.request.SearchRequest;
import fm.regions.processing.model.response.RegionResponse;
import fm.regions.processing.model.response.RegionsIdsResponse;
import fm.regions.processing.model.response.RegionsResponse;
import fm.regions.processing.service.api.RegionsFetchService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class RegionsFetchController extends BaseController implements RegionsFetchDocs {

    private final RegionsFetchService regionsFetchService;

    @Override
    public ResponseCoreEntity<RegionResponse> findRegionById(int regionId) {
        return response(regionsFetchService.findRegionById(regionId));
    }

    @Override
    public ResponseCoreEntity<RegionsResponse> findRegionsByIds(BatchRegionsRequestByIds request) {
        return response(regionsFetchService.findRegionsByIds(request.getRegionsIds()));
    }

    @Override
    public ResponseCoreEntity<RegionsResponse> findRegions(SearchRequest request) {
        return response(regionsFetchService.findRegions(request));
    }

    @Override
    public ResponseCoreEntity<RegionsIdsResponse> findRegionsIds(SearchRequest request) {
        return response(regionsFetchService.findRegionsIds(request));
    }
}
